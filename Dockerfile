FROM ubuntu:20.04

RUN apt-get update && apt-get install --no-install-recommends -y \
    curl \
    git  \
    ncdu \
    tig  \
    mc   \
    bat

# aliases
RUN echo 'alias ll="ls -l --group-directories-first --almost-all --color=auto"'  >> ~/.bashrc && \
    echo 'alias cat="batcat"' >> ~/.bashrc
