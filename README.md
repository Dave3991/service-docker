Simple docker image for doing service tasks at permission politicized servers -> fuck the rules.

### Usage:

#### If you are afraid:
```
docker run -it --rm -v /:/var/host:ro registry.gitlab.com/dave3991/service-docker/service_docker
```
this will mount whole host filesystem as **read only** into `/var/host` in container

#### If you know what are you doing:
```
docker run -it --rm -v /:/var/host:rw registry.gitlab.com/dave3991/service-docker/service_docker
```
this will mount whole host filesystem as **read write** into `/var/host` in container

### Tweaks inside:
coloful `ll` command:

![ll command](images/ll.png "ll command")

`cat` command:

![ll command](images/cat.png "ll command")
